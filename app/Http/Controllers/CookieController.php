<?php

namespace App\Http\Controllers;

use App\Http\Resources\RecipeResource;
use App\Models\Cookie;
use App\Services\CookieRecipeService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Symfony\Component\HttpFoundation\Request;

class CookieController extends Controller
{
    private CookieRecipeService $cookieRecipeService;
    public function __construct() {
        $this->cookieRecipeService = new CookieRecipeService();
    }

    /**
     * @return Application|Factory|View
     */
    public function index() {
        $cookie = new Cookie();
        $this->cookieRecipeService->addCookie($cookie)->generateOptimalScore();
        $bestRecipe = [
            'maxScore' => $this->cookieRecipeService->maxScore,
            'winningRecipe' => $this->cookieRecipeService->winningRecipe
        ];

        return view('cookie.index')->with(['bestRecipe' => $bestRecipe]);
    }

    /**
     * @param Request $request
     * @return RecipeResource
     */
    public function calorieCookie(Request $request): RecipeResource
    {
        $cookie = new Cookie();
        $this->cookieRecipeService->addCookie($cookie)->setLimitCalories((int)$request->get('calories') ?? 500)->generateOptimalScore();
        return RecipeResource::make($this->cookieRecipeService);
    }
}
