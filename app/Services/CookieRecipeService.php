<?php

namespace App\Services;

use App\Http\Resources\IngredientResource;
use App\Models\Cookie;
use App\Models\ingredient;
use Ramsey\Uuid\Type\Integer;

use function PHPUnit\Framework\stringContains;

class CookieRecipeService
{
    private Cookie $cookie;

    private $limitScoops = 100;

    private $limitCalories;

    public $winningRecipe = [];

    public $maxScore = 0;

    /**
     * @param mixed $limitCalories
     */
    public function setLimitCalories($limitCalories): CookieRecipeService
    {
        $this->limitCalories = $limitCalories;
        return $this;
    }

    /**
     * @param $cookie
     * @return $this
     */
    public function addCookie($cookie): CookieRecipeService
    {
        $this->cookie = $cookie;

        return $this;
    }

    public function setLimitScoops(Integer $limitScoops): CookieRecipeService
    {
        $this->limitScoops = $limitScoops;

        return $this;
    }

    /**
     * Try to generate the highest score possible.
     * Doing a dirty method of trying every combination possible
     */
    public function generateOptimalScore(): void
    {
        for ($capacity = 0; $capacity <= $this->limitScoops; $capacity++) {
            for ($durability = 0; $durability <= $this->limitScoops - $capacity; $durability++) {
                for ($flavor = 0; $flavor <= $this->limitScoops - $capacity - $durability; $flavor++) {
                    for ($texture = 0; $texture <= $this->limitScoops - $capacity - $durability - $flavor; $texture++) {
                        // I need this to stop the amount of heavy calculations
                        if ($capacity + $durability + $flavor + $texture != $this->limitScoops) {
                            continue;
                        }

                        $score = max(0, $this->cookie->ingredients['sprinkles']['capacity'] * $capacity + $this->cookie->ingredients['butterscotch']['capacity'] * $durability + $this->cookie->ingredients['chocolate']['capacity'] * $flavor + $this->cookie->ingredients['candy']['capacity'] * $texture);
                        $score *= max(0, $this->cookie->ingredients['sprinkles']['durability'] * $capacity + $this->cookie->ingredients['butterscotch']['durability'] * $durability + $this->cookie->ingredients['chocolate']['durability'] * $flavor + $this->cookie->ingredients['candy']['durability'] * $texture);
                        $score *= max(0, $this->cookie->ingredients['sprinkles']['flavor'] * $capacity + $this->cookie->ingredients['butterscotch']['flavor'] * $durability + $this->cookie->ingredients['chocolate']['flavor'] * $flavor + $this->cookie->ingredients['candy']['flavor'] * $texture);
                        $score *= max(0, $this->cookie->ingredients['sprinkles']['texture'] * $capacity + $this->cookie->ingredients['butterscotch']['texture'] * $durability + $this->cookie->ingredients['chocolate']['texture'] * $flavor + $this->cookie->ingredients['candy']['texture'] * $texture);

                        if (! is_null($this->limitCalories)) {
                            $calories  = $this->cookie->ingredients['sprinkles']['calories'] * $capacity + $this->cookie->ingredients['butterscotch']['calories'] * $durability + $this->cookie->ingredients['chocolate']['calories'] * $flavor + $this->cookie->ingredients['candy']['calories'] * $texture;
                            if ($calories !== $this->limitCalories) {
                                continue;
                            }

                        }
                        if ($score > $this->maxScore) {
                            $this->maxScore = $score;
                            $this->winningRecipe = [
                                'sprinkles' => $capacity,
                                'butterscotch' => $durability,
                                'chocolate' => $flavor,
                                'candy' => $texture,
                            ];
                        }
                    }
                }
            }
        }
    }
}
