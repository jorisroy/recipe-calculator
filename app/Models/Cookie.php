<?php

namespace App\Models;

use App\Http\Resources\IngredientResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class Cookie extends Model
{
    use HasFactory;

    public array $ingredients;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->ingredients = [
            'sprinkles' => new ingredient(['capacity' => 2, 'durability' => 0, 'flavor' => -2, 'texture' => 0, 'calories' => 3]),
            'butterscotch' => new ingredient(['capacity' => 0, 'durability' => 5, 'flavor' => -3, 'texture' => 0, 'calories' => 3]),
            'chocolate' => new ingredient(['capacity' => 0, 'durability' => 0, 'flavor' => 5, 'texture' => -1, 'calories' => 8]),
            'candy' => new ingredient(['capacity' => 0, 'durability' => -1, 'flavor' => 0, 'texture' => 5, 'calories' => 8]),
        ];
    }
}
