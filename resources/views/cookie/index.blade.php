@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ __('global.recipe.best') }}</h5>
                        @foreach($bestRecipe['winningRecipe'] as $name => $ingredient)
                            <div class="row">
                                <div class="col-md-6">
                                    {{ ucfirst($name) }}
                                </div>
                                <div class="col-md-6">
                                    {{ $ingredient }} teaspoons
                                </div>
                            </div>
                        @endforeach
                        <div class="row">
                            <div class="col-md-6">
                                Total points
                            </div>
                            <div class="col-md-6">
                                {{ $bestRecipe['maxScore'] }} Points
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <calorie-cookie-component></calorie-cookie-component>
            </div>
        </div>
    </div>
@endsection
