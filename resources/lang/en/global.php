<?php

return [
    'recipe' => [
        'best' => "The best recipe",
        'sprinkles' => 'Sprinkles',
        'butterscotch' => 'Butterscotch',
        'chocolate' => 'Chocolate',
        'candy' => 'Candy'
    ],
    'form' => [
        'name' => 'Name'
    ]
];
